﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Twitchel.Core;
using Twitchel.GUI;

namespace Twitchel
{
    public partial class MainWindow : Window
    {
        public event EventHandler<EventArgs> Exit;
        Action<object, SettingsAppliedEventArgs> settingsApplied; 
        TrayNotificator trayNotificator;
        private string title;
        private bool isExiting;

        public MainWindow()
        {
            isExiting = false;
            InitializeComponent();
            Icon = Imaging.CreateBitmapSourceFromHIcon(Properties.Resources.icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            App.GetCheckManager().checkStarted += OnCheckStarted;
            App.GetCheckManager().checkEnded += OnCheckEnded;
            InitializeTrayNotificator();
            title = App.APP_NAME + " ({0} Live)";
            #if DEBUG
                title = "[DEBUG] " + title;
            #endif
            this.Title = string.Format(title, 0);
            //updateEvery.Text = Properties.Settings.Default.updateDelay.ToString();
        }

        private void InitializeTrayNotificator()
        {
            trayNotificator = new TrayNotificator();

            trayNotificator.DoubleClicked += OnTrayNotificatorDoubleClick;
            trayNotificator.NotificationClicked += OnBaloonTipClicked;
            trayNotificator.SettingsClicked += (s, a) => OnSettingsClicked(s, null);
            trayNotificator.ExitClicked += OnTrayNotificatorExitClick;
        }

        public void ShowWinow()
        {
            if(!isExiting)
            {
                trayNotificator.Hide();
            }
            Show();
            this.Activate();
        }

        public void HideWinow()
        {
            if(!isExiting)
            {
                trayNotificator.Show();
            }
            Hide();
        }

        public void RegisterOnSettingsAccepted(Action<object, SettingsAppliedEventArgs> settingsApplied)
        {
            this.settingsApplied += settingsApplied;
        }

        void OnTrayNotificatorDoubleClick(object sender, EventArgs args)
        {
            ShowWinow();
        }

        void OnTrayNotificatorExitClick(object sender, EventArgs args)
        {
            OnExit(this, null);
        }

        void OnExit(object sender, RoutedEventArgs e)
        {
            isExiting = true;

            if(Exit != null)
            {
                Exit(this, null);
            }
            trayNotificator.Close();
        }

        private void OnWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            HideWinow();
        }
        
        public void OnLogin(object sender, RoutedEventArgs args)
        {
            Browser browser = new Browser();
            browser.browser.Navigate(TwitchApi.LOGIN_LINK);
            if(sender != null)
            {
                browser.Show();
            }
            browser.Icon = this.Icon;
            browser.browser.Navigated += Nav;
        }

        private void Nav(object sender, NavigationEventArgs e)
        {
            Logger.Debug(e.Uri.Host);
            if("secure.twitch.tv".Equals(e.Uri.Host) || "api.twitch.tv".Equals(e.Uri.Host))
            {
                OnLogin(null, null);
                Window.GetWindow(sender as WebBrowser).Close();
            }
            if("localhost".Equals(e.Uri.Host))
            {
                string[] response = e.Uri.OriginalString.Split(new char[] { '=', '&' });
                if(response.Length > 1 && response[0].EndsWith("#access_token"))
                {
                    Properties.Settings.Default.token = response[1];
                    Properties.Settings.Default.Save();
                    App.OnLogined();
                }
                Window window = Window.GetWindow(sender as WebBrowser);
                window.Close();
            }
        }

        private void OnCheckStarted(object sender, EventArgs args)
        {
            this.Dispatcher.Invoke(() =>
                {
                    //Lastpdate.Text = DateTime.Now.ToString("HH:mm:ss");
                    checkProgressBar.Visibility = Visibility.Visible;
                }
            );
        }

        private void OnBaloonTipClicked(object sender, EventArgs args)
        {
            this.ShowWinow();
        }

        private void OnCheckEnded(object sender, CheckEndEventArgs args)
        {
            this.Dispatcher.Invoke(() =>
                {
                    liveChannels.ItemsSource = args.streams;
                    this.Title = string.Format(title, args.streams.Count);
                    checkProgressBar.Visibility = Visibility.Hidden;
                }
            );
        }

        private void Image_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if(Properties.Settings.Default.noPrompt)
            {
                try
                {
                    System.Diagnostics.Process.Start((sender as FrameworkElement).Tag as string);
                }
                catch { }
            }
            else
            {
                Prompt prompt = new Prompt((sender as FrameworkElement).Tag as string);
                prompt.Icon = this.Icon;
                prompt.Owner = this;
                prompt.Show();
            }
        }

        private void OnSettingsClicked(object sender, RoutedEventArgs e)
        {
            Settings settings = new Settings();
            settings.SettingsApplied += (s, a) => { if(settingsApplied != null) settingsApplied(s, a); };
            settings.SettingsApplied += (s, a) => trayNotificator.UpdateMenuItemName();
            settings.Owner = this;
            settings.Show();
        }
    }
}
