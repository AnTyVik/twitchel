﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitchel.Core
{
    public class SettingsAppliedEventArgs : EventArgs
    {
        public bool isEnabled;
        public bool startup;
        public bool noPrompt;
        public int updateDelay;

        public SettingsAppliedEventArgs(bool isEnabled, bool startup, bool noPrompt, int updateDelay)
        {
            this.isEnabled = isEnabled;
            this.startup = startup;
            this.noPrompt = noPrompt;
            this.updateDelay = updateDelay;
        }
    }
}
