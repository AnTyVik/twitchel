﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Windows;
using System.Reflection;

namespace Twitchel.Core
{
    public struct StreamsResponse
    {
        public List<LiveStream> streams { get; set; }
        public int _total { get; set; }
        public Links _links { get; set; }
    }

    public struct TokenResponse : IDisposable
    {
        public Token token { get; set; }

        public void Dispose() {}
    }

    public struct LiveStream
    {
        public long _id { get; set; }
        public Channel channel { get; set; }
        public string game { get; set; }
        //public int viewers { get; set; }
        //public string created_at { get; set; }
        public Preview preview { get; set; }

        public override string ToString()
        {
            return channel.display_name + " (" + game + ")";
        }
    }

    public struct Channel
    {
        public string display_name { get; set; }
        public string status { get; set; }
        public string logo { get; set; }
        public string url { get; set; }
    }

    public struct Preview
    {
        public string small { get; set; }
        public string medium { get; set; }
        public string large { get; set; }
        //public string template { get; set; }
    }

    public struct Token
    {
        public bool valid { get; set; }
        public string user_name { get; set; }
    }

    public struct Links
    {
        public string self { get; set; }
        public string next { get; set; }
    }

    class TwitchApi
    {
        public const string LOGIN_LINK = @"https://api.twitch.tv/kraken/oauth2/authorize?response_type=token&client_id=4kyclrtmkh5ka6qbpyd1jxa6rzolu9c&redirect_uri=http://localhost&scope=user_read";

        private const string REQUEST_BASE = @"https://api.twitch.tv/kraken";

        private const string REQUEST_FOLLOWED_STREAMS = @"/streams/followed";

        private const string REQUEST_TOKEN = @"oauth_token=";

        private static async Task<T> GetResponseAsync<T>(string url)
        {
            HttpClient client = new HttpClient();
            Stream stream = await client.GetStreamAsync(url);
            StreamReader reader = new StreamReader(stream);
            JsonReader jsonReader = new JsonTextReader(reader);
            JsonSerializer serializer = new JsonSerializer();
            return (new JsonSerializer()).Deserialize<T>(jsonReader);
        }

        public static async Task<List<LiveStream>> GetFollowedStreams()
        {
            List<LiveStream> streams;
            try
            {
                StreamsResponse response = await GetResponseAsync<StreamsResponse>(REQUEST_BASE + REQUEST_FOLLOWED_STREAMS + "?" + REQUEST_TOKEN + Properties.Settings.Default.token/*url*/);
                streams = response.streams;
                int requests = (response._total / 25) + (response._total % 25 == 0 ? 0 : 1);

                for(int i = 0; i < requests; ++i)
                {
                    response = await GetResponseAsync<StreamsResponse>(response._links.next + "&" + REQUEST_TOKEN + Properties.Settings.Default.token);
                    streams.AddRange(response.streams);
                }
            }
            catch(Exception ex)
            {
                streams = new List<LiveStream>(0);
                MessageBox.Show(ex.ToString());
            }

            return streams;
        }

        public static async Task<TokenResponse> CheckToken()
        {
            return await GetResponseAsync<TokenResponse>(REQUEST_BASE + "?" + REQUEST_TOKEN + Properties.Settings.Default.token);
        }
    }
}
