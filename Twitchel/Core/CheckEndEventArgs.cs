﻿using System;
using Twitchel.Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitchel.Core
{
    public class CheckEndEventArgs : EventArgs
    {
        public List<LiveStream> streams;
    }
}
