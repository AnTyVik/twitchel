﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitchel.Core
{
    interface INotificator
    {
        void SendNotification(string title, string text, string icon, string snapshot);
    }
}
