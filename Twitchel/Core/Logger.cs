﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitchel.Core
{
    class Logger
    {
        public static void Log(string source, string level, object msg)
        {
            System.Diagnostics.Debug.WriteLine(msg, "[" + DateTime.Now.ToString("HH:mm:ss") + "][" + level + "][" + source + "]");
        }

        public static void Log(string level, object msg)
        {
            //TODO: [skip] Remove hardcoded source
            System.Diagnostics.Debug.WriteLine(msg, "[" + DateTime.Now.ToString("HH:mm:ss") + "][" + level + "][Twitchel]");
        }

        public static void Info(object msg)
        {
            Log("info", msg);
        }

        public static void Debug(object msg)
        {
            Log("debug", msg);
        }

        public static void Error(object msg)
        {
            Log("error", msg);
        }
    }
}
