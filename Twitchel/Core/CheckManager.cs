﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Twitchel.Core
{
    public class CheckManager
    {
        public event EventHandler<EventArgs> checkStarted;
        public event EventHandler<CheckEndEventArgs> checkEnded;
        public List<LiveStream> streams;
        private bool isChecking;

        public CheckManager()
        {
            streams = new List<LiveStream>();
            isChecking = false;
            App.Logined += (s, a) => DoCheck();
        }

        public async void Check()
        {
            while(true)
            {
                if(Properties.Settings.Default.isEnabled && !isChecking)
                {
                    DoCheck();
                }
                
                Thread.Sleep(Properties.Settings.Default.updateDelay * 60000);
            }
        }

        private async void DoCheck()
        {
            isChecking = true;
            Logger.Debug("CheckManager::DoCheck() >> Live check started");
            OnCheckStarted(App.Current, null);
            streams = await GetOnlineStreams();
            Logger.Debug("CheckManager::DoCheck() >> Live check ended");
            OnCheckEnded(App.Current, new CheckEndEventArgs { streams = streams });
            isChecking = false;
        }

        public bool IsTokenValid()
        {
            var task = Task.Run<TokenResponse>(() => TwitchApi.CheckToken());
            TokenResponse response = task.Result;
            return response.token.valid;
        }

        private  async Task<List<LiveStream>> GetOnlineStreams()
        {
            return await TwitchApi.GetFollowedStreams();
        }

        private  void OnCheckStarted(object sender, EventArgs args)
        {
            if(checkStarted != null)
            {
                checkStarted(sender, args);
            }
        }

        private  void OnCheckEnded(object sender, CheckEndEventArgs args)
        {
            if(checkEnded != null)
            {
                checkEnded(sender, args);
            }
        }
    }
}
