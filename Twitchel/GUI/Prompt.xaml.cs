﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Twitchel.GUI
{
    /// <summary>
    /// Interaction logic for Prompt.xaml
    /// </summary>
    public partial class Prompt : Window
    {
        private string url;

        public Prompt(string url)
        {
            InitializeComponent();
            this.url = url;
        }

        private void OnYesClick(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(url);
            }
            catch { }
            this.Close();
        }

        private void OnNoClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
