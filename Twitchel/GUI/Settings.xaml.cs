﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Twitchel.Core;

namespace Twitchel.GUI
{
    public partial class Settings : Window
    {
        //TODO: Add tooltips
        //TODO: Add clear button functionality
        public event EventHandler<SettingsAppliedEventArgs> SettingsApplied;
        public Settings()
        {
            InitializeComponent();
            textBoxDelay.Text = Properties.Settings.Default.updateDelay.ToString();
            checkBoxEnabled.IsChecked = Properties.Settings.Default.isEnabled;
            checkBoxStartup.IsChecked = Properties.Settings.Default.startup;
            checkBoxPrompt.IsChecked = Properties.Settings.Default.noPrompt;
            this.Icon = Imaging.CreateBitmapSourceFromHIcon(Properties.Resources.iconSettings.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            Logger.Debug(this.Owner + "|" + Owner == null);
        }

        private void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Logger.Debug(textBoxDelay.Text.Length);
            Regex regex = new Regex(@"\d");
            e.Handled = !regex.IsMatch(e.Text) || textBoxDelay.Text.Length > 2;
        }

        private void OnFocused(object sender, RoutedEventArgs e)
        {
            textBoxDelay.SelectAll();
        }

        private void OnClickedEnabled(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.isEnabled = checkBoxEnabled.IsChecked == true;
        }

        private void OnClickedStartup(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.startup = checkBoxStartup.IsChecked == true;
        }

        private void OnClickedPrompt(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.noPrompt = checkBoxPrompt.IsChecked == true;
        }

        private void OnSettingsApplied(bool isEnabled, bool startup, bool noPrompt, int delay)
        {
            if(SettingsApplied != null)
            {
                SettingsApplied(this, new SettingsAppliedEventArgs(isEnabled, startup, noPrompt, delay));
            }
        }

        private void OnClickedApply(object sender, RoutedEventArgs e)
        {
            int updateDelay = 0;
            int.TryParse(textBoxDelay.Text, out updateDelay);
            Properties.Settings.Default.updateDelay = updateDelay < 1 ? 1 : updateDelay > 120 ? 120 : updateDelay;
            //if(checkBoxStartup.IsChecked == true)
            //{
            //    App.AddToStartup();
            //}
            //else
            //{
            //    App.RemoveFromStartup();
            //}
            Properties.Settings.Default.Save();
            OnSettingsApplied(Properties.Settings.Default.isEnabled, Properties.Settings.Default.startup, Properties.Settings.Default.noPrompt, Properties.Settings.Default.updateDelay);
            this.Close();
        }

        private void OnClickedCancel(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Reload();
            this.Close();
        }
    }
}
