﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Twitchel.Core;

namespace Twitchel.GUI
{
    class TrayNotificator : INotificator
    {
        public event EventHandler<EventArgs> NotificationClicked;
        public event EventHandler<EventArgs> DoubleClicked;
        public event EventHandler<EventArgs> SettingsClicked;
        public event EventHandler<EventArgs> ExitClicked;

        private List<LiveStream> liveStreams;
        private NotifyIcon notifyIcon;
        private ContextMenu contextMenu;
        private MenuItem itemToggleEnable;
        private string iconText;
        private bool wasShownBeforeTooltip;

        public TrayNotificator()
        {
            iconText = App.APP_NAME + " ({0} Live)";//{1}
#if DEBUG
            iconText = "[DEBUG] " + iconText;
#endif

            contextMenu = new ContextMenu();
            itemToggleEnable = new MenuItem("");
            UpdateMenuItemName();
            contextMenu.MenuItems.Add(itemToggleEnable);
            contextMenu.MenuItems.Add("Settings", OnSettingsClick);
            contextMenu.MenuItems.Add("-");
            contextMenu.MenuItems.Add("Exit", OnExitClick);

            notifyIcon = new NotifyIcon();
            notifyIcon.Text = string.Format(iconText, "Noone is", "");
            notifyIcon.Icon = Properties.Resources.icon;
            notifyIcon.BalloonTipClosed += OnBalloonTipClosed;
            notifyIcon.BalloonTipClicked += OnBalloonTipClicked;

            App.GetCheckManager().checkEnded += OnCheckEnded;

            notifyIcon.MouseDoubleClick += OnDoubleClick;
            notifyIcon.ContextMenu = contextMenu;
            notifyIcon.Visible = true;
        }

        public void Show()
        {
            if(notifyIcon != null)
            {
                notifyIcon.Visible = true;
            }
        }

        public void Hide()
        {
            if(notifyIcon != null)
            {
                notifyIcon.Visible = false;
            }
        }

        public void Close()
        {
            notifyIcon.Dispose();
        }

        private void UpdateTitle(string newTitle)
        {
            notifyIcon.Text = newTitle;
        }

        public void SendNotification(string title, string text, string icon, string snapshot)
        {
            wasShownBeforeTooltip = notifyIcon.Visible;
            Show();
            notifyIcon.ShowBalloonTip(2000, title, text, ToolTipIcon.None);
        }

        public void UpdateMenuItemName()
        {
            itemToggleEnable.Text = Properties.Settings.Default.isEnabled ? "Disable" : "Enable";
        }

        private void OnToggleClick(object sender, EventArgs args)
        {
            Properties.Settings.Default.isEnabled = !Properties.Settings.Default.isEnabled;
            Properties.Settings.Default.Save();
            UpdateMenuItemName();
        }

        private void OnDoubleClick(object sender, MouseEventArgs args)
        {
            if(DoubleClicked != null)
            {
                DoubleClicked(this, null);
            }
        }

        private void OnSettingsClick(object sender, EventArgs args)
        {
            if(SettingsClicked != null)
            {
                SettingsClicked(this, null);
            }
        }

        private void OnExitClick(object sender, EventArgs args)
        {
            if(ExitClicked != null)
            {
                ExitClicked(this, null);
            }
        }

        private void OnCheckEnded(object sender, CheckEndEventArgs args)
        {
            //TODO: Add "Live" icon if someone streaming
            if(args.streams.Count > 0)
            {
                UpdateTitle(string.Format(iconText, args.streams.Count/*, lives*/));
                notifyIcon.Icon = Properties.Resources.iconLive;
            }
            else
            {
                UpdateTitle(string.Format(iconText, App.GetCheckManager().streams == null ? 0 : App.GetCheckManager().streams.Count));
                notifyIcon.Icon = Properties.Resources.icon;
            }
            foreach(LiveStream live in args.streams)
            {
                bool isNew = true;
                if(liveStreams != null)
                {
                    foreach(LiveStream prev in liveStreams)
                    {
                        if(live._id == prev._id)
                        {
                            isNew = false;
                        }
                    }
                }
                if(isNew)
                {
                    Logger.Debug("TrayNotificator::ShowNotification");
                    this.SendNotification(live.channel.display_name + " is streaming " + live.game, live.channel.status, live.channel.logo, live.preview.medium);
                }
            }
            liveStreams = new List<LiveStream>(args.streams);
        }

        private void OnBalloonTipClicked(object sender, EventArgs e)
        {
            if(NotificationClicked != null)
            {
                NotificationClicked(sender, e);
            }
        }

        private void OnBalloonTipClosed(object sender, EventArgs e)
        {
            //notifyIcon.Visible = wasShownBeforeTooltip;
        }
    }
}
