﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Twitchel.Core;

namespace Twitchel
{

    public partial class App : Application
    {
        public const string APP_NAME = "Twitchel";

        private const string REGISTRY_PATH = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";

        public static CheckManager checkManager;
        public static event EventHandler<EventArgs> Logined;

        static Thread checkThread;

        private MainWindow mainWindow;

        private void AppStartup(object sender, StartupEventArgs args)
        {
            //TODO: Add internet connection check
            
            ///Loads settings
            Twitchel.Properties.Settings.Default.Reload();

            AppDomain.CurrentDomain.AssemblyResolve += ResolveAssembly;

            ///Checks if need to run in backgroung (if launched on StartUp)
            bool runInBackground = args.Args.Length > 0 && "-bg".Equals(args.Args[0]);

            ///Creates CheckManager and Checking Thread
            SetupThread();

            //checkThread.Start();

            ///Add app to StartUp (if needed)
#if !DEBUG
            if(Twitchel.Properties.Settings.Default.startup)
            {
                AddToStartup();
            }
            else
            {
                RemoveFromStartup();
            }
#endif

            ///Creates instance of MainWindow
            mainWindow = new MainWindow();
            mainWindow.Exit += (s, e) => Shutdown(0);
            mainWindow.RegisterOnSettingsAccepted((s, a) => { if(a.startup) AddToStartup(); else RemoveFromStartup(); } );

            ///Checks if token exists and valid (and get it if needed) and runs program
            if(string.IsNullOrEmpty(Twitchel.Properties.Settings.Default.token) || !checkManager.IsTokenValid())
            {
                //Logined += (s, e) => checkThread.Start();
                Logined += (s, e) => StartWindow(runInBackground);
                mainWindow.OnLogin(this, null);
            }
            else
            {
                StartWindow(runInBackground);
            }
            //TODO: [skip] Decide if I need it
            //Exit += OnAppExit; 
        }

        private Assembly ResolveAssembly(object sender, ResolveEventArgs args)
        {
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            string name = args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll";
            string resourceName = thisAssembly.GetManifestResourceNames().First(s => s.EndsWith(name));

            using(Stream stream = thisAssembly.GetManifestResourceStream(resourceName))
            {
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);

                return Assembly.Load(buffer);
            }
        }

        public static CheckManager GetCheckManager()
        {
            return checkManager;
        }

        public static void OnLogined()
        {
            if(Logined != null)
            {
                Logined(null, null);
            }
        }

        public static void CheckLives()
        {
            if(checkThread.ThreadState == ThreadState.WaitSleepJoin)
            {
                checkThread.Resume();
            }
        }

        private void SetupThread()
        {
            checkManager = new CheckManager();
            checkThread = new Thread(checkManager.Check);
            checkThread.IsBackground = true;
            checkThread.Name = "Checking Thread";
        }

        private void StartWindow(bool runInBackground)
        {
            checkThread.Start();
            if(runInBackground)
            {
                mainWindow.HideWinow();
            }
            else
            {
                mainWindow.ShowWinow();
            }
        }

        private void AddToStartup()
        {
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            if(regKey.GetValue(APP_NAME) == null)
            {
                regKey.SetValue(APP_NAME, Assembly.GetExecutingAssembly().Location + " -bg");
            }
        }

        private void RemoveFromStartup()
        {
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            if(regKey.GetValue(APP_NAME) != null)
            {
                regKey.DeleteValue(APP_NAME);
            }
        }

        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        public static bool IsConnectedToInternet()
        {
            int Desc;
            return InternetGetConnectedState(out Desc, 0);
        }
    }
}
